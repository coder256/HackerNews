import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,ParamMap,Router } from '@angular/router';

import { NewsapiService } from '../newsapi.service';

@Component({
  selector: 'app-headlines',
  templateUrl: './headlines.component.html',
  styleUrls: ['./headlines.component.scss']
})
export class HeadlinesComponent implements OnInit {
  source:string;
  headlines: string[];
  error:boolean = false;
  errorMessage:string = "";

  constructor(
    private newsapi: NewsapiService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    //get params
    this.source = this.route.snapshot.paramMap.get('source');
    console.log(`The source :: ${this.source}`);

    this.newsapi.getHeadlines(this.source).subscribe(
      data => {
        console.log("The headlines" , data);
        this.headlines = data.articles;
      },
      err => {
        this.error = true;
        this.errorMessage = err.statusText;
      }
    );
  }

}
