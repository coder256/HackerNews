import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { MomentModule } from 'angular2-moment';


import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { StoriesComponent } from './stories/stories.component';
import { FooterComponent } from './footer/footer.component';
import { ItemComponent } from './item/item.component';
import { HackernewsApiService } from './hackernews-api.service';
import { DomainPipe } from './domain.pipe';
import { CommentsComponent } from './comments/comments.component';
import { RoutesModule } from './routes.module';
import { HeadlinesComponent } from './headlines/headlines.component';
import { NewsapiService } from './newsapi.service';
import { SourcesComponent } from './sources/sources.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    StoriesComponent,
    FooterComponent,
    ItemComponent,
    DomainPipe,
    CommentsComponent,
    HeadlinesComponent,
    SourcesComponent
  ],
  imports: [
    BrowserModule,HttpClientModule,MomentModule, RoutesModule
  ],
  providers: [HackernewsApiService, NewsapiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
