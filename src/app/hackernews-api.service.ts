import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
//import 'rxjs/add/operator/map';
import { map,tap } from 'rxjs/operators';

@Injectable()
export class HackernewsApiService {
  //baseUrl: string;
  //this.baseUrl = 'https://hacker-news.firebaseio.com/v0';
  private baseUrl = 'https://node-hnapi.herokuapp.com';

  constructor(
    private http: HttpClient) {}

  fetchStories_old(): Observable<any>{
    return this.http.get(`${this.baseUrl}/topstories.json`).pipe(
      tap(_ => console.log("i got stories"))
    );
  }
  fetchStories(storyType: string, page: number): Observable<any>{
    return this.http.get(`${this.baseUrl}/${storyType}?page=${page}`).pipe(
      tap(_ => console.log("Got stories from new api"))
    );
  }

  fetchItem(id: number): Observable<any>{
    return this.http.get(`${this.baseUrl}/item/${id}.json`).pipe(
      tap(_ => console.log("Got item details"))
    );
  }

}
