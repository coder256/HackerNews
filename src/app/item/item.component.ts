import { Component, OnInit, Input } from '@angular/core';

import { HackernewsApiService} from '../hackernews-api.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {
  @Input() itemID: number;
  @Input() item;

  constructor() { }

  ngOnInit() {
    //this.hackerNewsApiService.fetchItem(this.itemID)
    //  .subscribe(
    //  data => {
    //    console.log("The item story details ::", data);
    //    this.item = data;
    //  },
    //  error => {
    //    console.log("failed to fetch story details");
    //  }
    //);
  }

}
