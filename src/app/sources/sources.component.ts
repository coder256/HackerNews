import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sources',
  templateUrl: './sources.component.html',
  styleUrls: ['./sources.component.scss']
})
export class SourcesComponent implements OnInit {
  sources = [
    {"name":"BBC News","code": "bbc-news"},
    {"name":"BBC Sport","code": "bbc-sport"},
    {"name":"Bloomberg","code": "bloomberg"},
    {"name":"Business Insider","code": "business-insider"},
    {"name":"Breitbart News","code": "breitbart-news"},
    {"name":"Buzzfeed","code": "buzzfeed"},
    {"name":"CNN","code": "cnn"},
    {"name":"Entertainment Weekly","code": "entertainment-weekly"},
    {"name":"FourFour Two","code": "four-four-two"},
    {"name":"Google News","code": "google-news"},
    {"name":"Mashable","code": "mashable"},
    {"name":"National Geographic","code": "national-geographic"},
    {"name":"Reddit /r/all","code": "reddit-r-all"},
    {"name":"Techcrunch","code": "techcrunch"},
  ];

  constructor() { }

  ngOnInit() {
  }

}
