import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { StoriesComponent } from './stories/stories.component';
import { CommentsComponent } from './comments/comments.component';
import { SourcesComponent } from './sources/sources.component';
import { HeadlinesComponent } from './headlines/headlines.component';

const routes: Routes = [
  {path: '', redirectTo: 'news/1',pathMatch: 'full'},
  {path: 'news/:page', component: StoriesComponent, data: {storiesType: 'news'}},
  {path: 'newest/:page', component: StoriesComponent, data: {storiesType: 'newest'}},
  {path: 'show/:page', component: StoriesComponent, data: {storiesType: 'show'}},
  {path: 'ask/:page', component: StoriesComponent, data: {storiesType: 'ask'}},
  {path: 'jobs/:page', component: StoriesComponent, data: {storiesType: 'jobs'}},
  {path: 'comments/:id', component: CommentsComponent},
  {path: 'sources', component: SourcesComponent},
  {path: 'headlines/:source', component: HeadlinesComponent}
];

@NgModule({
  exports: [
    RouterModule
  ],
  imports : [ RouterModule.forRoot(routes) ]
})
export class RoutesModule { }
