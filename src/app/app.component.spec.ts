//import { TestBed, async } from '@angular/core/testing';
//import { AppComponent } from './app.component';
//describe('AppComponent', () => {
//  beforeEach(async(() => {
//    TestBed.configureTestingModule({
//      declarations: [
//        AppComponent
//      ],
//    }).compileComponents();
//  }));
//  it('should create the app', async(() => {
//    const fixture = TestBed.createComponent(AppComponent);
//    const app = fixture.debugElement.componentInstance;
//    expect(app).toBeTruthy();
//  }));
//  it(`should have as title 'app'`, async(() => {
//    const fixture = TestBed.createComponent(AppComponent);
//    const app = fixture.debugElement.componentInstance;
//    expect(app.title).toEqual('app');
//  }));
//  it('should render title in a h1 tag', async(() => {
//    const fixture = TestBed.createComponent(AppComponent);
//    fixture.detectChanges();
//    const compiled = fixture.debugElement.nativeElement;
//    expect(compiled.querySelector('h1').textContent).toContain('Welcome to app!');
//  }));
//});

import { ComponentFixture, TestBed,async } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { AppComponent } from './app.component';

describe('AppComoponent (template url)',() => {
  let comp: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let de: DebugElement;
  let el: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AppComponent] //declare the testing component
    })
    .compileComponents(); //compile template and css
  }));
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations:[AppComponent] //declare the test component
    });

    fixture = TestBed.createComponent(AppComponent);

    comp = fixture.componentInstance; //app component test instance

    //query for the title
    de = fixture.debugElement.query(By.css('h1'));
    el = de.nativeElement;
  });

  it('should display original title',() => {
    fixture.detectChanges();
    expect(el.textContent).toContain(comp.title);
  });

  it('should display a different test title', () => {
    comp.title = 'Test Title';
    fixture.detectChanges();
    expect(el.textContent).toContain('Test Title');
  });
});
