import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,ParamMap,Router } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import { Location } from '@angular/common';

import { HackernewsApiService} from '../hackernews-api.service';

@Component({
  selector: 'app-stories',
  templateUrl: './stories.component.html',
  styleUrls: ['./stories.component.scss']
})
export class StoriesComponent implements OnInit {
  items: number[];
  typeSub: any;
  pageSub: any;
  storiesType: string;
  pageNum: number;
  listStart: number;

  error: boolean = false;
  errorMessage: string;

  constructor(
    private hackerNewsApiService: HackernewsApiService,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location
  ) {}

  ngOnInit() {
    //this.items = Array(30);

    //get params
    this.typeSub = this.route.data.
      subscribe(data => {
        //console.log('the data',data);
        this.storiesType = data.storiesType;
      });
    this.pageSub = this.route.snapshot.paramMap.get('page');
    this.pageNum = +this.route.snapshot.paramMap.get('page');
    console.log(`TypeSub : ${this.storiesType} and pageSub : ${this.pageSub}`);

    this.fetchStories(this.storiesType,this.pageNum);
  }

  fetchStories(storiestype: string, pageNum: number): void{
    console.log(`Fetching stories page :: ${pageNum}`);
    this.hackerNewsApiService.fetchStories(storiestype,pageNum)
      .subscribe(
      //items => this.items = items
        data => {
        //console.log("the response ::" + typeof data + " === ", data);
        this.items = data;
      },
        err => {
          console.log("Error occured ::",err);
          this.error = true;
          this.errorMessage = err.statusText;
      },
      () => {
        this.listStart = ((this.pageNum - 1) * 30) + 1;
        console.log(`List start :: ${this.listStart}`);
        window.scrollTo(0,0);
      }
    );
  }

  prev(): void {
    this.pageNum -= 1;
    this.router.navigate([`/${this.storiesType}/${this.pageNum}`]);
    this.items = null;
    this.fetchStories(this.storiesType,this.pageNum);
  }
  next(): void {
    this.pageNum += 1;
    this.router.navigate([`/${this.storiesType}/${this.pageNum}`]);
    //window.location.reload();
    this.items = null;
    this.fetchStories(this.storiesType,this.pageNum);
  }

}
