import { Injectable } from '@angular/core';
import { HttpClient,HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map,tap } from 'rxjs/operators';

import { Constants } from './constants';

@Injectable()
export class NewsapiService {

  constructor(private http: HttpClient) { }

  getSources() :Observable<any>{
    let query_params = new HttpParams();
    query_params = query_params.append("apiKey",Constants.api_key);

    return this.http.get(`${Constants.api_url}/sources`,{
      params: query_params
    });
  }

  getHeadlines(source:string): Observable<any>{
    let query_params = new HttpParams();
    query_params = query_params.append("apiKey",Constants.api_key);
    query_params = query_params.append("sources",source);

    return this.http.get(`${Constants.api_url}/top-headlines`,{
      params: query_params
    });
  }

}
