/**
 * Created by Andrew on 11/21/17.
 */
//importScripts('/cache-polyfill.js');

self.addEventListener('install',function(e){
  e.waitUntil(
    caches.open('airhorner').then(function(cache){
      return cache.addAll([
        '/',
        '/app.component.html',
        '/app.component.scss',
        '/app.component.ts',
        '/app.module.ts'
      ]);
    })
  );
});
