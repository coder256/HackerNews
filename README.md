# HackerNews Client

HackerNews is a hacker news client built to as a progressive web app and is powered by Angular5 and the awesome angular CLI. It fetches news from an [unofficial hacker news api] (https://github.com/cheeaun/node-hnapi) and is hosted on firebase [here](angular2-hn-30b0f.firebaseapp.com).

This was done following the awesome [angular2 tutorial](https://houssein.me/angular2-hacker-news) with some adjustments added for angular5.

The pwa elements are added following https://angularfirebase.com/lessons/installable-angular-progressive-web-app/

## To run:
- **ng serve --open**  --this will serve the app on port 4200 and open it in your default browser. Use this command during development. App will automatically reload if save any changes to the files.
- To build the app for deployment to a server run **ng build --prod** This will create a dist folder that has your final compiled app.
- To learn more about the angular cli checkout [Angular CLI](https://github.com/angular/angular-cli)
